.PHONY: run
run:
	@echo "-- Starting stack in docker compose"
	@docker compose -p predict up --remove-orphans

.PHONY: stop
stop:
	@echo "-- Stopping stack"
	@docker compose -p predict down
