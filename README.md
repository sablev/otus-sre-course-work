# Прогнозирование временных рядов

Проектная работа по курсу «SRE практики и инструменты», OTUS

Март-сентябрь 2023 г.

## Цели

Продемонстрировать применение практик и инструментов SRE для решения задач проектной работы.

## Задачи

* [x] Выполнить прогнозирование временного ряда при помощи [Prophet](https://facebook.github.io/prophet/)
* [x] Обеспечить отображение фактических и прогнозных значений на дэшборде [Grafana](https://grafana.com/grafana/)

## Работы

### Прогнозирование временных рядов

* [x] Поиск, получение и подготовка массива данных
  * [x] [Поиск и получение массива данных наблюдений за погодой](./docs/1-dataset-find.md)
  * [x] [Подготовка массива данных для задачи прогнозирования](./docs/2-dataset-prepare.md)
* [x] Обучение модели и прогнозирование
  * [x] [Выбор данных для обучения модели и проверки прогноза](./docs/3-data-choose.md)
  * [x] [Обучение модели, прогнозирование и экспорт временных рядов](./docs/4-learn-and-export-ts.md)
* [x] Представление временных рядов в Grafana
  * [x] [Подготовка метрик и загрузка в Prometheus](./docs/5-backfill-metrics-to-tsdb.md)
  * [x] [Cоздание дэшборда Grafana](./docs/6-generate-grafana-dashboard.md)

### Исследование влияния параметров запуска Prometheus на retention-политику

* [x] [Планирование, автоматизация и проведения эксперимента](https://gitlab.com/sablev/prometheus-retention-test)


## Решение

### Прогнозирование временных рядов

#### Jupyter-ноутбуки

1. [Предварительная подготовка сведений о метеостанциях](./solution/1-prepare_stations_data.ipynb)
2. [Предварительная подготовка данных о наблюдениях погоды](./solution/2-prepare_ts_data.ipynb)
3. [Подготовка данных к загрузке в БД](./solution/3-prepare_data_to_db.ipynb)
4. [Загрузка данных в БД](./solution/4-load_data_to_db.ipynb)
5. [Обучение модели и подготовка временных рядов](./solution/5-learn.ipynb)
6. [Подготовка метрик и загрузка в TSDB](./solution/6-backfill_metrics.ipynb)
7. [Создание дэшборда Grafana](./solution/7-grafana-dashboard.ipynb)

#### Данные

1. [Исходный массив данных](./solution/dataset-raw/)
2. [Обработанный массив данных](./solution/dataset/)
3. [БД наблюдений погоды](./solution/db/)
   1. [Список стран](./solution/db/countries.csv)
   2. [Список метеостанций](./solution/db/stations.csv)
   3. [Данные наблюдений погоды](./solution/db/observations/)
4. [Временные ряды](./solution/ts/)
   1. [Исторические значения](./solution/ts/historical.csv)
   2. [Прогнозные значения](./solution/ts/forecast.csv)
5. [Метрики в openmetrics-формате](./solution/openmetrics/24679.txt)

### Исследование влияния параметров запуска Prometheus на retention-политику

#### Jupyter-ноутбуки

1. [Исследование влияния параметров запуска Prometheus на retention-политику](https://gitlab.com/sablev/prometheus-retention-test/-/blob/main/test.ipynb) 
