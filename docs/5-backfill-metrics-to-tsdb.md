# Подготовка метрик и загрузка в Prometheus

## Проектирование

Температура может меняться в любую сторону, поэтому метрике подходит тип `gauge`:

| № | Наименование | Тип | Описание |
| --- | --- | --- | --- |
| 1 | air_temperature_celsius | gauge | Среднесуточное значение температуры воздуха в градусах Цельсия |

Параметры:

| Наименование | Описание                   | Примеры значений                                                        |
|--------------|----------------------------|------------------------------------------------------------------------|
| kind         | Вид значения               | `min` — минимальное, ``mean`` — среднее, `max` — максимальное                  |
| type         | Тип значения               | `observation` — наблюдение, `forecast` — прогноз                           |
| station_id   | Идентификатор метеостанции | `24679`                                                                  |
| source       | Источник сведений          | `meteo_ru` — для наблюдений, `prophet_defaults` — для прогнозных значений  |

## Подготовка метрик

Загрузка ретроспективных значений метрик в Prometheus возможна двумя способами — при помощи утилиты promtool и посредством remote write протокола.

Был выбран способ загрузки при помощи утилиты promtool из файла в openmetrics-формате:

```plain
# HELP air_temperature_celsius Daily air temperature in Celsius degrees.
# TYPE air_temperature_celsius gauge
air_temperature_celsius{kind="mean",type="observation",station_id="24679",source="meteo_ru"} -37.5 1609459200
air_temperature_celsius{kind="mean",type="forecast",station_id="24679",source="prophet_defaults"} -33.7 1640908800
# EOF
```

Преобразование временных рядов в openmetrics-формат ([24679.txt](../solution/openmetrics/24679.txt)) для загрузки в Prometheus реализовано в Jupyter-ноутбуке ([6-backfill_metrics.ipynb](../solution/6-backfill_metrics.ipynb)).

## Загрузка метрик

Команда для загрузки метрик:

```console
promtool tsdb create-blocks-from openmetrics /openmetrics/24679.txt /prometheus/data
```
