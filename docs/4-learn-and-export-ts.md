# Обучение модели, прогнозирование и экспорт временных рядов

Для обучения модели и прогнозирования использовался [Prophet](https://facebook.github.io/prophet/).

## Реализация

### Jupyter-ноутбуки

* [5-learn.ipynb](../solution/5-learn.ipynb)

## Результаты

Временные ряды среднесуточных значений температуры:

* [исторические значения](../solution/ts/24679-observation-meteo_ru.csv)
* [прогнозные значения](../solution/ts/24679-forecast-prophet_defaults.csv)