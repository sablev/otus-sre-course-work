# Подготовка массива данных для задачи прогнозирования

В ходе подготовки массива данных для прогнозирования, выполнены:

* преобразование данных в пригодный для обработки формат,
* очистка данных от ошибочных сведений,
* проектирование БД для загрузки данных,
* подготовка данных к загрузке в БД,
* загрузка данных в БД для удобства анализа,
* анализ качества данных,
* выбраны данные для использования в качестве обучающей и проверочной выборки.

## Реализация

### Jupyter-ноутбуки

* [1-prepare_stations_data.ipynb](../solution/1-prepare_stations_data.ipynb)
* [2-prepare_ts_data.ipynb](../solution/2-prepare_ts_data.ipynb)
* [3-prepare_data_to_db.ipynb](../solution/3-prepare_data_to_db.ipynb)
* [4-load_data_to_db.ipynb](../solution/4-load_data_to_db.ipynb)


### БД

#### Структура

```plantuml

entity countries {
    * id : int
    --
    * name : varchar(64)
}

entity stations {
    * id : int
    --
    * country_id : int
    * name : varchar(128)
}

entity observations {
    * station_id : int
    * created_at : date
    --
    * tflag : quality_level
    * qtmin : quality_level
    * qtmean : quality_level
    * qtmax : quality_level
    * cr : precipitation_feature
    * qr : quality_level
    tmin : numeric(5,1)
    tmean : numeric(5,1)
    tmax : numeric(5,1)
    r : numeric(5,1)
}

countries ||--o{ stations
stations ||--o{ observations
```

#### DDL

```sql
CREATE TYPE quality_level AS enum ('true', 'mismatched', 'false');

CREATE TYPE precipitation_feature AS enum ('true', 'multiple days', 'zero', 'few', 'false');

CREATE TABLE countries(
    id INT PRIMARY KEY,
    name VARCHAR(64) NOT NULL
);

COMMENT ON TABLE countries IS 'Страны';
COMMENT ON COLUMN countries.id IS 'Идентификатор страны';
COMMENT ON COLUMN countries.name IS 'Название страны';

CREATE TABLE stations(
    id INT PRIMARY KEY,
    country_id INT NOT NULL,
    name VARCHAR(128) NOT NULL
);

COMMENT ON TABLE stations IS 'Метеостанции';
COMMENT ON COLUMN stations.id IS 'Идентификатор метеостанции';
COMMENT ON COLUMN stations.country_id IS 'Ссылка на страну';
COMMENT ON COLUMN stations.name IS 'Название метеостанции';

CREATE TABLE observations(
    station_id INT NOT NULL,
    created_at DATE NOT NULL,
    tflag quality_level NOT NULL,
    tmin NUMERIC(5, 1),
    qtmin quality_level NOT NULL,
    tmean NUMERIC(5, 1),
    qtmean quality_level NOT NULL,
    tmax NUMERIC(5, 1),
    qtmax quality_level NOT NULL,
    r NUMERIC(5, 1),
    cr precipitation_feature NOT NULL,
    qr quality_level NOT NULL,
    PRIMARY KEY(station_id, created_at)
);

COMMENT ON TABLE observations IS 'Наблюдения';
COMMENT ON COLUMN observations.station_id IS 'Идентификатор метеостанции';
COMMENT ON COLUMN observations.created_at IS 'Дата наблюдения';
COMMENT ON COLUMN observations.tflag IS 'Групповой признак качества для показателей температуры воздуха';
COMMENT ON COLUMN observations.qtmin IS 'Признак качества минимальной температуры воздуха за сутки';
COMMENT ON COLUMN observations.qtmean IS 'Признак качества среднесуточной температуры воздуха';
COMMENT ON COLUMN observations.qtmax IS 'Признак качества максимальной температуры воздуха за сутки';
COMMENT ON COLUMN observations.tmin IS 'Минимальная температура воздуха за сутки';
COMMENT ON COLUMN observations.tmean IS 'Среднесуточная температура воздуха';
COMMENT ON COLUMN observations.tmax IS 'Максимальная температура воздуха за сутки';
COMMENT ON COLUMN observations.r IS 'Суточная сумма осадков';
COMMENT ON COLUMN observations.cr IS 'Дополнительный признак суточной суммы осадков';
COMMENT ON COLUMN observations.qr IS 'Признак качества суточной суммы осадков';
```

### quality_level

| Код | Значение | Описание |
| --- | --- | --- |
| 0 | 'true' | Значение достоверно |
| 1 | 'mismatched' | Значение не согласуется с данными архива срочных наблюдений |
| 9 | 'false' | Значение забраковано или наблюдения не проводились |

### precipitation_feature

| Код | Значение | Описание |
| --- | --- | --- |
| 0 | 'true' | Измеренное количество осадков 0,1 мм и более |
| 1 | 'multiple days' | Осадки измерены за несколько дней |
| 2 | 'zero' | Измерения осадков производились, но осадков не было (R = 0) |
| 3 | 'few' | Наблюдались только следы осадков (< 0,1 мм) (R = 0) |
| 9 | 'false' | Значение забраковано или наблюдения не проводились |
