# Проблема загрузки исторических данных в Prometheus

## Описание проблемы

1. Используется версия Prometheus v2.46.0:

    ```plain
    prometheus:
        container_name: prometheus
        image: prom/prometheus:v2.46.0
    ```

    Возможность подлива метрик была реализована в версии v2.24.0.

2. Для загрузки используется файл со значениями метрик в формате openmetrics из примера:

    ```plain
    # HELP http_requests_total The total number of HTTP requests.
    # TYPE http_requests_total counter
    http_requests_total{code="200",service="user"} 123 1609954636
    http_requests_total{code="500",service="user"} 456 1609954730
    # EOF
    ```

3. Значения метрики `http_requests_total` за 06.01.2021.

4. Для загрузки применяется команда:

    ```console
    promtool tsdb create-blocks-from openmetrics ./metrics/test.txt
    ```

5. В результате исполнения команды в файловой системе создаётся новый блок.

6. После рестарта Prometheus успешно находит новый блок:

   ```plain
   ts=2023-08-23T17:43:27.080Z caller=repair.go:56 level=info component=tsdb msg="Found healthy block" mint=1609954636000 maxt=1609954730001 ulid=01H8HQVR1WHHWFS3706Z4EQVVE
   ```

   и сразу же его удаляет:

   ```plain
   ts=2023-08-23T17:43:27.238Z caller=db.go:1617 level=info component=tsdb msg="Deleting obsolete block" block=01H8HQVR1WHHWFS3706Z4EQVVE
   ```

7. Retention-политика при этом не предполагает такого поведения ни по времени, ни по размеру:

   ```plain
    - '--storage.tsdb.retention.time=5y'
    - '--storage.tsdb.retention.size=1GB'
   ```

8. Такое же поведение и при «свежих» таймштампах:

    ```plain
    # HELP http_requests_total The total number of HTTP requests.
    # TYPE http_requests_total counter
    http_requests_total{code="200",service="user"} 123 1692814415
    http_requests_total{code="500",service="user"} 456 1692814421
    # EOF
    ```

## Похожие проблемы у других пользователей

* [Problem backfilling with promtool](https://discuss.prometheus.io/t/problem-backfilling-with-promtool/1234)
* [Cannot see metrics values after loading through create-blocks-from](https://groups.google.com/g/prometheus-users/c/_ApSUhgBSb8)