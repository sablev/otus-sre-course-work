# Выбор данных для обучения модели и проверки прогноза

## Анализ

### Сколько всего наблюдений в год по каждой метеостанции?

```sql
SELECT
    station_id,
    extract(year from created_at) AS year,
    COUNT(*) AS count
FROM
    observations
GROUP BY
    station_id,
    year
ORDER BY
    station_id,
    year;
```

### У каких метеостанций и в какие годы наибольшее кол-во качественных наблюдений?

```sql
SELECT
    station_id,
    extract(year from created_at) AS year,
    COUNT(*) AS count
FROM
    observations
WHERE
    tflag != 'true'
GROUP BY
    station_id,
    year
ORDER BY
    count ASC,
    year DESC;
```

## Выводы

Одни из самых качественных данных температуры — на метеостанции __Восточная__:

```sql
SELECT * FROM stations WHERE id = 24679;
```

Запрос кол-ва качественных наблюдений температуры на станции __Восточная__ по годам:

```sql
SELECT
    station_id,
    extract(year from created_at) AS year,
    COUNT(*) AS count
FROM
    observations
WHERE
    station_id = 24679
    AND tflag = 'true'
GROUP BY
    station_id,
    year
ORDER BY
    year DESC,
    count DESC;
```

## Выборки

### Выборка для обучения

Запрос формирования выборки для обучения модели (фактическая среднесуточная температура воздуха за 2016-2020 гг.):

```sql
SELECT
    created_at,
    tmean
FROM observations
WHERE
    station_id = 24679
    AND extract(year from created_at) >= 2016
    AND extract(year from created_at) <= 2020;
```

### Выборка для проверки качества обучения

Запрос формирования выборки для проверки качества обучения модели (фактическая среднесуточная температура воздуха за 2021 г.):

```sql
SELECT
    created_at,
    tmean
FROM observations
WHERE
    station_id = 24679
    AND extract(year from created_at) >= 2016
    AND extract(year from created_at) <= 2020;
```
